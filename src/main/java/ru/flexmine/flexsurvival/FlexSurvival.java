package ru.flexmine.flexsurvival;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;
import ru.dymeth.flexdonate.FlexDonatePlugin;
import ru.flexmine.flexsurvival.fixes.PearlsGlitchFix;
import ru.flexmine.flexsurvival.fixes.SpawnersPlaceFix;
import ru.flexmine.flexsurvival.fixes.WorldBorderFix;
import ru.flexmine.flexsurvival.mechanics.SpawnerEggs;
import ru.flexmine.flexsurvival.mechanics.DifficultyController;
import ru.flexmine.flexsurvival.mechanics.GriefMechanics;
import ru.flexmine.flexsurvival.mechanics.enchant.MainAutoEnchant;

import java.io.File;

public class FlexSurvival extends JavaPlugin {
    @Override
    public void onEnable() {
        String serverCategory = FlexDonatePlugin.getServerName().split("-")[0];
        boolean warnOnNewChunks;
        boolean griefMechanics;
        Difficulty worldsDifficulty;
        switch (serverCategory) {
            case "surv":
                warnOnNewChunks = true;
                griefMechanics = true;
                worldsDifficulty = Difficulty.NORMAL;
                break;
            case "hard":
                warnOnNewChunks = true;
                griefMechanics = true;
                worldsDifficulty = Difficulty.HARD;
                break;
            case "ob":
                warnOnNewChunks = false;
                griefMechanics = false;
                worldsDifficulty = Difficulty.NORMAL;
                break;
            case "dg":
                warnOnNewChunks = true;
                griefMechanics = false;
                worldsDifficulty = Difficulty.NORMAL;
                break;
            case "hub":
                warnOnNewChunks = true;
                griefMechanics = false;
                worldsDifficulty = Difficulty.PEACEFUL;
                break;
            default:
                throw new IllegalArgumentException("Не удалось найти конфигурурацию для серверов типа " + serverCategory);
        }

        //Загрузка конфигов
        ConfigurationSection enchants = YamlConfiguration.loadConfiguration(new File(this.getDataFolder(), "enchants.yml"));


        // Фиксы
        this.getServer().getPluginManager().registerEvents(new PearlsGlitchFix(), this);
        this.getServer().getPluginManager().registerEvents(new SpawnersPlaceFix(this), this);
        this.getServer().getPluginManager().registerEvents(new WorldBorderFix(this, warnOnNewChunks), this);
        this.getServer().getPluginManager().registerEvents(new SpawnerEggs(this), this);

        // Механики
        Bukkit.getServer().getPluginManager().registerEvents(new DifficultyController(this, worldsDifficulty), this);
        if (griefMechanics)
            Bukkit.getServer().getPluginManager().registerEvents(new GriefMechanics(), this);

        //загрузка дополнительных
        MainAutoEnchant mainAutoEnchant = new MainAutoEnchant(this);
    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
    }
}
