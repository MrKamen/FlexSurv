package ru.flexmine.flexsurvival.mechanics.enchant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.enchantments.Enchantment;

@RequiredArgsConstructor
@Getter
public class PurchasedEnchant {
    private final Enchantment enchantment;
    private final int enchantLvl;
    private final int price;
}
