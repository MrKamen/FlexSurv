package ru.flexmine.flexsurvival.mechanics.enchant.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.flexmine.flexsurvival.mechanics.enchant.menu.MainMenu;

public class EnchantCommand implements CommandExecutor {
    private MainMenu mainMenu;

    public EnchantCommand(MainMenu mainMenu) {
        this.mainMenu = mainMenu;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Команда выполняется из игры");
            return false;
        }

        Player player = (Player) sender;
        player.openInventory(mainMenu.getInventory());

        return true;
    }
}
