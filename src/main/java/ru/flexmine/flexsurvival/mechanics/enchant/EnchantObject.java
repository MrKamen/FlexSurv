package ru.flexmine.flexsurvival.mechanics.enchant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import ru.flexmine.flexsurvival.mechanics.enchant.menu.EnchantLvlSelectorMenu;

import java.util.*;

@Getter
public class EnchantObject {
    private final Enchantment enchant;
    private final String name;
    private List<String> lore = new ArrayList<>();
    private List<Integer> prices = new ArrayList<>();
    private Set<Category> categories = new HashSet<>();
    private EnchantLvlSelectorMenu enchantLvlSelector;

    public EnchantObject(Enchantment enchant, String name) {
        this.enchant = enchant;
        this.name = ChatColor.LIGHT_PURPLE + name;
    }

    public EnchantObject setLore(String... lore) {
        this.lore.addAll(Arrays.asList(lore));
        return this;
    }

    public EnchantObject setPrices(Integer... prices) {
        this.prices.addAll(Arrays.asList(prices));
        return this;
    }

    public EnchantObject setCategories(Category... categories) {
        this.categories.addAll(Arrays.asList(categories));
        return this;
    }
}
