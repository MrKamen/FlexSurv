package ru.flexmine.flexsurvival.mechanics.enchant;

import org.bukkit.enchantments.Enchantment;
import ru.flexmine.flexsurvival.FlexSurvival;
import ru.flexmine.flexsurvival.mechanics.enchant.Listener.Listener;
import ru.flexmine.flexsurvival.mechanics.enchant.commands.EnchantCommand;
import ru.flexmine.flexsurvival.mechanics.enchant.menu.CategoryMenu;
import ru.flexmine.flexsurvival.mechanics.enchant.menu.MainMenu;

import java.util.*;

public class MainAutoEnchant {
    private FlexSurvival plugin;
    private Set<EnchantObject> enchants = new HashSet<>();
    private MainMenu mainMenu;
    private List<CategoryMenu> categoryMenus = new ArrayList<>();

    public MainAutoEnchant(FlexSurvival plugin) {
        this.plugin = plugin;
        fillEnchants(); // заполнение массива
        categoryMenus.add(new CategoryMenu(Category.WEAPON, this.enchants));
        categoryMenus.add(new CategoryMenu(Category.ARMOR, this.enchants));
        categoryMenus.add(new CategoryMenu(Category.TOOL, this.enchants));
        this.mainMenu = new MainMenu(categoryMenus);

        this.plugin.getServer().getPluginManager().registerEvents(new Listener(this.mainMenu), this.plugin);
        this.plugin.getCommand("autoenchant").setExecutor(new EnchantCommand(this.mainMenu));
    }

    private void fillEnchants() {
        this.enchants.add(new EnchantObject(Enchantment.ARROW_INFINITE, "Бесконечность")
                .setPrices(25)
                .setCategories(Category.WEAPON));
        this.enchants.add(new EnchantObject(Enchantment.DAMAGE_ALL, "Острота")
                .setPrices(10, 20, 30, 40, 50)
                .setCategories(Category.WEAPON));
        this.enchants.add(new EnchantObject(Enchantment.ARROW_FIRE, "Горящая стрела")
                .setPrices(35, 50)
                .setCategories(Category.WEAPON));
        this.enchants.add(new EnchantObject(Enchantment.ARROW_DAMAGE, "Сила Лука")
                .setPrices(10, 20, 25, 34, 40)
                .setCategories(Category.WEAPON));
        this.enchants.add(new EnchantObject(Enchantment.ARROW_KNOCKBACK, "Откидывание Луком")
                .setPrices(50, 80)
                .setCategories(Category.WEAPON));
        this.enchants.add(new EnchantObject(Enchantment.LOOT_BONUS_MOBS, "Добыча")
                .setPrices(30, 60, 110)
                .setCategories(Category.WEAPON));
        this.enchants.add(new EnchantObject(Enchantment.MENDING, "Починка")
                .setPrices(50)
                .setCategories(Category.WEAPON, Category.ARMOR, Category.TOOL));
        this.enchants.add(new EnchantObject(Enchantment.DAMAGE_ARTHROPODS, "Бич Членестоногих")
                .setPrices(10, 20, 35, 50, 75)
                .setCategories(Category.WEAPON));
        this.enchants.add(new EnchantObject(Enchantment.SWEEPING_EDGE, "Разящий Клинок")
                .setPrices(10, 20, 35, 50, 75)
                .setCategories(Category.WEAPON));
        this.enchants.add(new EnchantObject(Enchantment.FIRE_ASPECT, "Заговор Огня")
                .setPrices(10, 20)
                .setCategories(Category.WEAPON));
        this.enchants.add(new EnchantObject(Enchantment.KNOCKBACK, "Откидывание")
                .setPrices(50, 80)
                .setCategories(Category.WEAPON));
        this.enchants.add(new EnchantObject(Enchantment.DURABILITY, "Прочность")
                .setPrices(8, 15, 24)
                .setCategories(Category.WEAPON, Category.ARMOR, Category.TOOL));
        this.enchants.add(new EnchantObject(Enchantment.DAMAGE_UNDEAD, "Небесная Кара")
                .setPrices(10, 20, 30, 45, 70)
                .setCategories(Category.WEAPON));
        this.enchants.add(new EnchantObject(Enchantment.PROTECTION_FIRE, "Огнестойкость")
                .setPrices(10, 20, 30, 45)
                .setCategories(Category.ARMOR));
        this.enchants.add(new EnchantObject(Enchantment.FROST_WALKER, "Ледоход")
                .setPrices(10, 18)
                .setCategories(Category.ARMOR));
        this.enchants.add(new EnchantObject(Enchantment.WATER_WORKER, "Подводник")
                .setPrices(20, 30, 45, 56, 75)
                .setCategories(Category.ARMOR));
        this.enchants.add(new EnchantObject(Enchantment.OXYGEN, "Подводное дыхание")
                .setPrices(10, 18, 26 ,45, 75)
                .setCategories(Category.ARMOR));
        this.enchants.add(new EnchantObject(Enchantment.PROTECTION_ENVIRONMENTAL, "Защита")
                .setPrices(4, 12, 20, 36, 45)
                .setCategories(Category.ARMOR));
        this.enchants.add(new EnchantObject(Enchantment.PROTECTION_PROJECTILE, "Защита от снарядов")
                .setPrices(10, 20, 30, 45, 56)
                .setCategories(Category.ARMOR));
        this.enchants.add(new EnchantObject(Enchantment.PROTECTION_EXPLOSIONS, "Взрывоустойчивость")
                .setPrices(10, 20, 30, 45, 56)
                .setCategories(Category.ARMOR));
        this.enchants.add(new EnchantObject(Enchantment.PROTECTION_FALL, "Невесомость")
                .setPrices(15, 38, 65, 90)
                .setCategories(Category.ARMOR));
        this.enchants.add(new EnchantObject(Enchantment.THORNS, "Шипы")
                .setPrices(15, 35, 60)
                .setCategories(Category.ARMOR));
        this.enchants.add(new EnchantObject(Enchantment.DEPTH_STRIDER, "Подводная хотьба")
                .setPrices(15, 28, 45)
                .setCategories(Category.ARMOR));
        this.enchants.add(new EnchantObject(Enchantment.LOOT_BONUS_BLOCKS, "Удача")
                .setPrices(35, 70, 130)
                .setCategories(Category.TOOL));
        this.enchants.add(new EnchantObject(Enchantment.LUCK, "Везучий рыбак")
                .setPrices(15, 35, 55, 76, 90)
                .setCategories(Category.TOOL));
        this.enchants.add(new EnchantObject(Enchantment.LURE, "Приманка")
                .setPrices(20, 30, 50)
                .setCategories(Category.TOOL));
        this.enchants.add(new EnchantObject(Enchantment.DIG_SPEED, "Эффективность")
                .setPrices(8, 15, 28, 45, 80)
                .setCategories(Category.TOOL));
        this.enchants.add(new EnchantObject(Enchantment.SILK_TOUCH, "Шёлковое касание")
                .setPrices(35)
                .setCategories(Category.TOOL));
    }

}
