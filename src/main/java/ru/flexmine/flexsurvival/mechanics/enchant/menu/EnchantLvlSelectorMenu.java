package ru.flexmine.flexsurvival.mechanics.enchant.menu;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import ru.flexmine.flexsurvival.mechanics.enchant.EnchantObject;
import ru.flexmine.flexsurvival.mechanics.enchant.PurchasedEnchant;
import ru.flexmine.flexsurvival.utils.ItemUtils;

import java.util.HashMap;
import java.util.Map;

@Getter
public class EnchantLvlSelectorMenu implements InventoryHolder {
    private final Inventory inv;
    private final EnchantObject enchant;
    private final Map<Integer, PurchasedEnchant> purchasedEnchant = new HashMap<>();

    public EnchantLvlSelectorMenu(EnchantObject enchant) {
        this.enchant = enchant;
        inv = Bukkit.createInventory(this, 54, "Зачарования | " + ChatColor.stripColor(this.enchant.getName()));
        this.inv.setItem(9 * 5 - 1 + 1, ItemUtils.createItem(Material.BARRIER, "&cНазад"));
        this.inv.setItem(9 * 5 - 1 + 4, ItemUtils.createItem(Material.DOUBLE_PLANT,
                "&6ФлексКоины",
                " ",
                "§6◾ §fФлексКоины являются донат-валютой",
                "§6◾ §fПокупайте собственное §eколичество §fфлекскоинов",
                "§6◾ §fЧем §eбольше §fпокупаете, тем §eбольше §fбонусов при покупке!",
                " ",
                "§6◾ §fКупить ФлексКоины можно на сайте §b§lFlexMine.ru",
                " ",
                "§6➦ §7Нажмите, чтобы купить"
        ));
        this.inv.setItem(9 * 5 - 1 + 6, ItemUtils.createItem(Material.PAPER, "&aКак оплатить",
                "§7Покупку ФлексКоинов можно произвести через:",
                " ",
                "§6◾ §fQiwi, WebMoney, Яндекс Деньги, Альфа-Банк",
                "§6◾ §fVisa, Mastercard, Maestro, Мир",
                "§6◾ §fБилайн, МТС, Мегафон, Билайн",
                " ",
                "§7Выберите интересующее Вас количество флекскоинов",
                "§7и оплатите их любым удобным способом"
        ));
        setDuplicateItems(ItemUtils.createItem(Material.STAINED_GLASS_PANE, 3, " "),
                0, 1, 2, 3, 4, 5, 6, 7, 8,
                9, 17,
                18, 26,
                27, 35,
                36, 37, 38, 39, 40, 41, 42, 43, 44,
                46, 47, 51, 52, 53);
        setDuplicateItems(ItemUtils.createItem(Material.STAINED_GLASS_PANE, 6, " "),
                10, 11, 15, 16,
                19, 25,
                28, 29, 30, 31, 32, 33, 34);
        this.fill();
    }

    private void fill() {
        int index = 20;
        for (int i = 0; i < this.enchant.getEnchant().getMaxLevel(); i++) {
            String name = this.enchant.getName() + " " + (i + 1);
            int price;
            if (this.enchant.getPrices().isEmpty()) {
                throw new RuntimeException("Нет цен на зачарование " + this.enchant.getName());
            } else {
                if (this.enchant.getPrices().size() < (i + 1)) {
                    price = this.enchant.getPrices().get(0);
                    System.out.println("зачарование " + this.enchant.getName() + " лвл " + (i + 1) + " цена " + price);
                    System.out.println("Нет цены, зачарование " + this.enchant.getName() + " лвл " + (i + 1));
                } else {
                    price = this.enchant.getPrices().get(i);
                }
            }
            this.inv.setItem(index, ItemUtils.createItem(Material.ENCHANTED_BOOK, name, ("§fЦена " + "§e" + price + " ФлексКоинов")));
            this.purchasedEnchant.put(index, new PurchasedEnchant(this.enchant.getEnchant(), (i + 1), price));
            index++;
        }
    }

    private void setDuplicateItems(ItemStack panel, Integer... indices) {
        for (Integer index : indices) {
            this.inv.setItem(index, panel);
        }
    }

    @Override
    public Inventory getInventory() {
        return inv;
    }
}
