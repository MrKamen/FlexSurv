package ru.flexmine.flexsurvival.mechanics.enchant.menu;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import ru.flexmine.flexsurvival.mechanics.enchant.Category;
import ru.flexmine.flexsurvival.utils.ItemUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
public class MainMenu implements InventoryHolder {
    private final Inventory inv = Bukkit.createInventory(this, 54, "Зачарования");
    private final List<CategoryMenu> categoryMenus;
    private final Map<Integer, CategoryMenu> index = new HashMap<>();

    public MainMenu(List<CategoryMenu> categoryMenus) {
        this.categoryMenus = categoryMenus;
        this.inv.setItem(9 * 5 - 1 + 1, ItemUtils.createItem(Material.BARRIER, "&cЗакрыть меню"));
        this.inv.setItem(9 * 5 - 1 + 4, ItemUtils.createItem(Material.DOUBLE_PLANT,
                "&6ФлексКоины",
                " ",
                "§6◾ §fФлексКоины являются донат-валютой",
                "§6◾ §fПокупайте собственное §eколичество §fфлекскоинов",
                "§6◾ §fЧем §eбольше §fпокупаете, тем §eбольше §fбонусов при покупке!",
                " ",
                "§6◾ §fКупить ФлексКоины можно на сайте §b§lFlexMine.ru",
                " ",
                "§6➦ §7Нажмите, чтобы купить"
                ));
        this.inv.setItem(9 * 5 - 1 + 6, ItemUtils.createItem(Material.PAPER, "&aКак оплатить",
                "§7Покупку ФлексКоинов можно произвести через:",
                " ",
                "§6◾ §fQiwi, WebMoney, Яндекс Деньги, Альфа-Банк",
                "§6◾ §fVisa, Mastercard, Maestro, Мир",
                "§6◾ §fБилайн, МТС, Мегафон, Билайн",
                " ",
                "§7Выберите интересующее Вас количество флекскоинов",
                "§7и оплатите их любым удобным способом"
        ));
        setDuplicateItems(ItemUtils.createItem(Material.STAINED_GLASS_PANE, 3, " "),
                0, 1, 2, 3, 4, 5, 6, 7, 8,
                9, 10, 16, 17,
                18, 26,
                27, 35,
                36, 37, 38, 39, 40, 41, 42, 43, 44,
                46, 47, 51, 52, 53);
        this.fill();
    }

    private void setDuplicateItems(ItemStack panel, Integer... indices) {
        for (Integer index : indices) {
            this.inv.setItem(index, panel);
        }
    }

    private void fill() {
        int index = 20;
        for (CategoryMenu categoryMenu : categoryMenus) {
            Category category = categoryMenu.getCategory();
            this.inv.setItem(index, ItemUtils.createItem(category.getMaterialIcon(), category.getName(), category.getLore()));
            this.index.put(index, categoryMenu);
            index = index + 2;
        }
    }

    @Override
    public Inventory getInventory() {
        return inv;
    }
}
