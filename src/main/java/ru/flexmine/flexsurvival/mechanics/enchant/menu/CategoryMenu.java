package ru.flexmine.flexsurvival.mechanics.enchant.menu;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.flexmine.flexsurvival.mechanics.enchant.Category;
import ru.flexmine.flexsurvival.mechanics.enchant.EnchantObject;
import ru.flexmine.flexsurvival.utils.ItemUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
public class CategoryMenu implements InventoryHolder {
    private final Inventory inv;
    private final Category category;
    private Set<EnchantObject> enchants;
    private final Map<Integer, EnchantLvlSelectorMenu> indexEnchant = new HashMap<>();

    public CategoryMenu(Category category, Set<EnchantObject> enchants) {
        this.category = category;
        this.setEnchants(enchants);
        inv = Bukkit.createInventory(this, 54, "Зачарования | " + ChatColor.stripColor(this.category.getName()));
        this.inv.setItem(9 * 5 - 1 + 1, ItemUtils.createItem(Material.BARRIER, "&cНазад"));
        this.inv.setItem(9 * 5 - 1 + 4, ItemUtils.createItem(Material.DOUBLE_PLANT,
                "&6ФлексКоины",
                " ",
                "§6◾ §fФлексКоины являются донат-валютой",
                "§6◾ §fПокупайте собственное §eколичество §fфлекскоинов",
                "§6◾ §fЧем §eбольше §fпокупаете, тем §eбольше §fбонусов при покупке!",
                " ",
                "§6◾ §fКупить ФлексКоины можно на сайте §b§lFlexMine.ru",
                " ",
                "§6➦ §7Нажмите, чтобы купить"
        ));
        this.inv.setItem(9 * 5 - 1 + 6, ItemUtils.createItem(Material.PAPER, "&aКак оплатить",
                "§7Покупку ФлексКоинов можно произвести через:",
                " ",
                "§6◾ §fQiwi, WebMoney, Яндекс Деньги, Альфа-Банк",
                "§6◾ §fVisa, Mastercard, Maestro, Мир",
                "§6◾ §fБилайн, МТС, Мегафон, Билайн",
                " ",
                "§7Выберите интересующее Вас количество флекскоинов",
                "§7и оплатите их любым удобным способом"
        ));
        setDuplicateItems(ItemUtils.createItem(Material.STAINED_GLASS_PANE, 3, " "),
                0, 1, 2, 3, 4, 5, 6, 7, 8,
                9, 17,
                18, 26,
                27, 35,
                36, 44,
                46, 47, 51, 52, 53);
        this.fill(); // заполнение категории книгами
    }

    private void setDuplicateItems(ItemStack panel, Integer... indices) {
        for (Integer index : indices) {
            this.inv.setItem(index, panel);
        }
    }

    private void fill() {
        if (enchants == null || enchants.size() > 7 * 3) {
            this.sendError("Зачарований больше, чем в меню свободного места");
            return;
        }
        this.enchants.forEach(enchant -> {
            ItemStack book = new ItemStack(Material.ENCHANTED_BOOK);
            ItemMeta bookMeta = book.getItemMeta();
            bookMeta.setDisplayName(enchant.getName());
            bookMeta.setLore(enchant.getLore());
            book.setItemMeta(bookMeta);
            int firstEmpty = this.inv.firstEmpty();
            this.inv.setItem(firstEmpty, book);
            this.indexEnchant.put(firstEmpty, new EnchantLvlSelectorMenu(enchant));
        });
    }

    private void setEnchants(Set<EnchantObject> enchants) {
        if (enchants == null) {
            this.sendError("Enchants == null");
            return;
        }
        this.enchants = enchants.stream()
                .filter(enchant -> enchant.getCategories().contains(this.category))
                .collect(Collectors.toSet());
    }

    private void sendError(String error) {
        System.out.println("Категория " + this.category.toString() + " не может быть заполнена.");
        System.out.println(error);
    }

    @Override
    public Inventory getInventory() {
        return this.inv;
    }
}
