package ru.flexmine.flexsurvival.mechanics.enchant.Listener;

import lombok.RequiredArgsConstructor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.dymeth.flexdonate.FlexDonatePlugin;
import ru.dymeth.flexdonate.product.StoreProduct;
import ru.dymeth.flexdonate.product.StoreProductBuilder;
import ru.flexmine.flexsurvival.mechanics.enchant.PurchasedEnchant;
import ru.flexmine.flexsurvival.mechanics.enchant.menu.CategoryMenu;
import ru.flexmine.flexsurvival.mechanics.enchant.menu.EnchantLvlSelectorMenu;
import ru.flexmine.flexsurvival.mechanics.enchant.menu.MainMenu;

import java.util.Map;

@RequiredArgsConstructor
public class Listener implements org.bukkit.event.Listener {
    private final MainMenu mainMenu;

    @EventHandler
    public void on(InventoryClickEvent e) {
        Inventory inventory = e.getClickedInventory();
        if (inventory == null) return;

        if (e.getInventory().getHolder() instanceof MainMenu) {
            e.setCancelled(true);
            clickMainMenu(e);
        } else if (e.getInventory().getHolder() instanceof CategoryMenu) {
            e.setCancelled(true);
            clickCategoryMenu(e);
        } else if (e.getInventory().getHolder() instanceof EnchantLvlSelectorMenu) {
            e.setCancelled(true);
            clickEnchantLvlSelectorMenu(e);
        } else return;
    }

    private void clickMainMenu(InventoryClickEvent e) {
        int slot = e.getRawSlot();
        Player player = (Player) e.getWhoClicked();
        MainMenu menu = (MainMenu) e.getInventory().getHolder();
        if (slot == 45) player.closeInventory(InventoryCloseEvent.Reason.PLAYER);
        Map<Integer, CategoryMenu> index = menu.getIndex();
        if (!index.containsKey(slot)) return;
        CategoryMenu categoryMenu = index.get(slot);
        player.openInventory(categoryMenu.getInventory());
        return;
    }

    private void clickCategoryMenu(InventoryClickEvent e) {
        int slot = e.getRawSlot();
        Player player = (Player) e.getWhoClicked();
        CategoryMenu menu = (CategoryMenu) e.getInventory().getHolder();
        if (slot == 45) player.openInventory(this.mainMenu.getInventory());
        Map<Integer, EnchantLvlSelectorMenu> enchantSelectors = menu.getIndexEnchant();
        if (!enchantSelectors.containsKey(slot)) return;
        if (!menu.getCategory().getAllowedMaterials().contains(player.getInventory().getItemInMainHand().getType())) {
            player.sendMessage("Этот предмет нельзя зачаровать в данной категории");
        } else {
            player.openInventory(enchantSelectors.get(slot).getInventory());
        }
        return;
    }

    private void clickEnchantLvlSelectorMenu(InventoryClickEvent e) {
        int slot = e.getRawSlot();
        Player player = (Player) e.getWhoClicked();
        EnchantLvlSelectorMenu menu = (EnchantLvlSelectorMenu) e.getInventory().getHolder();
        if (slot == 45) player.openInventory(this.mainMenu.getInventory());
        Map<Integer, PurchasedEnchant> purchasedEnchant = menu.getPurchasedEnchant();
        if (!purchasedEnchant.containsKey(slot)) return;
        PurchasedEnchant enchant = purchasedEnchant.get(slot);
        ItemStack item = player.getInventory().getItemInMainHand();
        if (enchant.getEnchantment().canEnchantItem(item)) {
            StoreProduct product = new StoreProductBuilder("FlexSurv-Enchant", menu.getEnchant().getName())
                    .setDonatePrice(enchant.getPrice())
                    .setOnDeal(buyer -> {
                        item.addUnsafeEnchantment(enchant.getEnchantment(), enchant.getEnchantLvl());
                    })
                    .build();
            FlexDonatePlugin.createTransaction(player, product, 1);
        } else {
            player.sendMessage("Извините, но данное зачарование не предназначено для данного предмета");
        }
        return;
    }
}
