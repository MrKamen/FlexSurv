package ru.flexmine.flexsurvival.mechanics.enchant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.ChatColor;
import org.bukkit.Material;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

@Getter
@RequiredArgsConstructor
public enum Category {
    WEAPON("§6Зачарования для оружия",
            Material.DIAMOND_SWORD,
            Arrays.asList(" ",
                    "§6◾ §fЗачарований: §613",
                    "§6◾ §fСоздай идеальное оружие!",
                    "§6◾ Меч, топор и лук!",
                    " ",
                    "§6➦ §7Нажми, чтобы открыть"),
            EnumSet.of(Material.DIAMOND_SWORD, Material.GOLD_SWORD, Material.IRON_SWORD, Material.STONE_SWORD, Material.WOOD_SWORD,
                    Material.DIAMOND_AXE, Material.GOLD_AXE, Material.IRON_AXE, Material.STONE_AXE, Material.WOOD_AXE,
                    Material.BOW)),
    ARMOR("§6Зачарования для брони",
            Material.IRON_CHESTPLATE,
            Arrays.asList(" ",
                    "§6◾ §fЗачарований: §612",
                    "§6◾ §fСоздай идеальную броню!",
                    " ",
                    "§6➦ §7Нажми, чтобы открыть"),
            EnumSet.of(Material.DIAMOND_HELMET, Material.DIAMOND_CHESTPLATE, Material.DIAMOND_LEGGINGS, Material.DIAMOND_BOOTS,
                    Material.IRON_HELMET, Material.IRON_CHESTPLATE, Material.IRON_LEGGINGS, Material.IRON_BOOTS,
                    Material.CHAINMAIL_HELMET, Material.CHAINMAIL_CHESTPLATE, Material.CHAINMAIL_LEGGINGS, Material.CHAINMAIL_BOOTS,
                    Material.GOLD_HELMET, Material.GOLD_CHESTPLATE, Material.GOLD_LEGGINGS, Material.GOLD_BOOTS,
                    Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.LEATHER_BOOTS)),
    TOOL("§6Зачарования для инструментов",
            Material.DIAMOND_PICKAXE,
            Arrays.asList(" ",
                    "§6◾ §fЗачарований: §67",
                    "§6◾ §fСоздай идеальный инструмент!",
                    "§6◾ Кирка, топор и лопата, мотыга и удочка!",
                    " ",
                    "§6➦ §fНажми, чтобы открыть"),
            EnumSet.of(Material.DIAMOND_PICKAXE, Material.DIAMOND_AXE, Material.DIAMOND_SPADE, Material.DIAMOND_HOE,
                    Material.IRON_PICKAXE, Material.IRON_AXE, Material.IRON_SPADE, Material.IRON_HOE,
                    Material.GOLD_PICKAXE, Material.GOLD_AXE, Material.GOLD_SPADE, Material.GOLD_HOE,
                    Material.STONE_PICKAXE, Material.STONE_AXE, Material.STONE_SPADE, Material.STONE_HOE,
                    Material.WOOD_PICKAXE, Material.WOOD_AXE, Material.WOOD_SPADE, Material.WOOD_HOE,
                    Material.FISHING_ROD));

    private final String name;
    private final Material materialIcon;
    private final List<String> lore;
    private final EnumSet<Material> allowedMaterials;

}
