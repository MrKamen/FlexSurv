package ru.flexmine.flexsurvival.mechanics;

import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryType;

import java.util.ArrayList;
import java.util.List;

public class GriefMechanics implements Listener {
    private final List<Block> blownUpBlocks = new ArrayList<>();

    @EventHandler(priority = EventPriority.LOW)
    private void onLow(EntityExplodeEvent event) {
        if (event.getEntityType() != EntityType.WITHER_SKULL && event.getEntityType() != EntityType.WITHER) return;
        this.blownUpBlocks.clear();
        this.blownUpBlocks.addAll(event.blockList());
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    private void onHigh(EntityExplodeEvent event) {
        if (event.getEntityType() != EntityType.WITHER_SKULL && event.getEntityType() != EntityType.WITHER) return;
        if (this.blownUpBlocks.size() == 0) return;
        // Некорректная проверка, поскольку иссушитель может
        // сломать часть блоков, которые находятся вне привата
        // if (event.blockList().size() != 0) return;
        // Игнорируем com.sk89q.worldguard.bukkit.listener.EventAbstractionListener с приоритетом NORMAL
        event.blockList().addAll(this.blownUpBlocks);
        this.blownUpBlocks.clear();
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void on(BlockPistonRetractEvent event) {
        event.setCancelled(false);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void on(BlockPistonExtendEvent event) {
        event.setCancelled(false);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void on(InventoryMoveItemEvent event) {
        if (event.getDestination().getType() != InventoryType.HOPPER) return;
        event.setCancelled(false);
    }
}
