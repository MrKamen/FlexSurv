package ru.flexmine.flexsurvival.mechanics;

import de.tr7zw.nbtapi.NBTItem;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import ru.flexmine.flexsurvival.FlexSurvival;

public class SpawnerEggs implements Listener {
	private final FlexSurvival plugin;

	public SpawnerEggs(FlexSurvival plugin) {
		this.plugin = plugin;
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
	private void on(PlayerInteractEvent event) {
		Block block = event.getClickedBlock();
		if (block == null || block.getType() != Material.MOB_SPAWNER) return;
		ItemStack hand = event.getItem();
		if (hand == null || hand.getType() != Material.MONSTER_EGG) return;
		if (new NBTItem(hand).hasKey("modify_spawners")) {
			 event.getPlayer().sendMessage(ChatColor.GREEN + "Вы сменили тип моба в спаунере");
		} else {
			event.getPlayer().sendMessage(ChatColor.RED + "Вы можете использовать только покупные яйца!");
			event.setCancelled(true);
		}
	}
}
