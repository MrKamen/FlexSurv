package ru.flexmine.flexsurvival.mechanics;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;
import ru.flexmine.flexsurvival.FlexSurvival;

public class DifficultyController implements Listener {
    private final Difficulty worldsDifficulty;

    public DifficultyController(FlexSurvival plugin, Difficulty worldsDifficulty) {
        this.worldsDifficulty = worldsDifficulty;
        for (World world : Bukkit.getWorlds())
            world.setDifficulty(this.worldsDifficulty);
        plugin.getServer().getScheduler().runTask(plugin, () -> {
            for (World world : Bukkit.getWorlds())
                world.setDifficulty(this.worldsDifficulty);
        });
    }

    @EventHandler
    private void on(WorldLoadEvent event) {
        event.getWorld().setDifficulty(this.worldsDifficulty);
    }
}
