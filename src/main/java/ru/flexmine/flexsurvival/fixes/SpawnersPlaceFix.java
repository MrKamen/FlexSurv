package ru.flexmine.flexsurvival.fixes;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import ru.flexmine.flexsurvival.FlexSurvival;

public class SpawnersPlaceFix implements Listener {
    private final FlexSurvival plugin;

    public SpawnersPlaceFix(FlexSurvival plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    private void on(BlockPlaceEvent event) {
        if (event.getPlayer() == null) return;
        ItemStack hand = event.getItemInHand();
        if (hand == null || hand.getType() != Material.MOB_SPAWNER) return;
        Block block = event.getBlockPlaced();
        if (new ItemStack(block.getType(), 1).getType() != Material.MOB_SPAWNER) return;
        BlockState blockState = block.getState();
        if (!(blockState instanceof CreatureSpawner)) return;

        CreatureSpawner blockSpawner = (CreatureSpawner) blockState;
        CreatureSpawner handSpawner = (CreatureSpawner) ((BlockStateMeta) hand.getItemMeta()).getBlockState();
        this.applySpawnerOptions(handSpawner, blockSpawner);
    }

    private void applySpawnerOptions(CreatureSpawner from, CreatureSpawner to) {
        // TODO Клонировать все характеристики автоматически
        to.setSpawnedType(from.getSpawnedType());
        to.setDelay(from.getDelay());
        to.setMinSpawnDelay(from.getMinSpawnDelay());
        to.setMaxSpawnDelay(from.getMaxSpawnDelay());
        to.setSpawnCount(from.getSpawnCount());
        to.setMaxNearbyEntities(from.getMaxNearbyEntities());
        to.setRequiredPlayerRange(from.getRequiredPlayerRange());
        to.setSpawnRange(from.getSpawnRange());
        // TODO Применять все SpawnPotentials
        to.update();
    }
}
