package ru.flexmine.flexsurvival.fixes;

import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import ru.flexmine.flexsurvival.FlexSurvival;

public class WorldBorderFix implements Listener {
    private final FlexSurvival plugin;
    private final boolean warnOnNewChunks;

    public WorldBorderFix(FlexSurvival plugin, boolean warnOnNewChunks) {
        this.plugin = plugin;
        this.warnOnNewChunks = warnOnNewChunks;
    }

    @EventHandler
    private void on(ChunkLoadEvent event) {
        if (!this.warnOnNewChunks) return;
        if (!event.isNewChunk()) return;
        if (event.getWorld().getEnvironment() != World.Environment.NORMAL) return;
        this.warn("Сгенерирован чанк за границей мира: " + event.getChunk().getX() + ";" + event.getChunk().getZ());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void on(PlayerMoveEvent event) {
        if (event.getTo().getWorld().getWorldBorder().isInside(event.getTo())) return;
        this.warn("Игрок " + event.getPlayer().getName()
                + " попытался попасть за границу мира: " + event.getFrom() + " -> " + event.getTo());
        event.setCancelled(true);
    }

    private void warn(String message) {
        new IllegalStateException("[" + this.plugin.getName() + "] " + message).printStackTrace();
    }
}
