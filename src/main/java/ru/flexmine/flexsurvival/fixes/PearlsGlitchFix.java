package ru.flexmine.flexsurvival.fixes;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.*;

public class PearlsGlitchFix implements Listener {
    private static final boolean ALLOW_MULTIPLE_PEARLS = true;
    private final Set<Player> pearlsLaunchPositions = new HashSet<>();
    private Location sourceLoc = null;
    private boolean locChanged = false;

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    private void on(ProjectileLaunchEvent event) {
        Projectile projectile = event.getEntity();
        if (!(projectile instanceof EnderPearl)) return;
        if (!(projectile.getShooter() instanceof Player)) return;
        if (ALLOW_MULTIPLE_PEARLS) return;

        Player player = (Player) projectile.getShooter();
        if (this.pearlsLaunchPositions.add(player)) return;
        event.setCancelled(true);
        player.setCooldown(Material.ENDER_PEARL, 3);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void on(ProjectileHitEvent event) {
        Projectile projectile = event.getEntity();
        if (!(projectile instanceof EnderPearl)) return;
        if (!(projectile.getShooter() instanceof Player)) return;

        Player player = (Player) projectile.getShooter();
        this.pearlsLaunchPositions.remove(player);

        this.sourceLoc = player.getLocation();
        this.locChanged = false;
        Location targetLoc = this.sourceLoc.clone();

        // Base
        if (isWall(0.0, 1.0, 0.0)) {
            targetLoc.add(targetLoc.getDirection().multiply(-1.0));
            targetLoc.add(targetLoc.getDirection().multiply(-1.0));
        }
        if (isWall(0.0, 0.0, 0.0))
            targetLoc.add(0.0, 0.5, 0.0);

        // X
        if (isWall(1.0, 0.0, 0.0) || isWall(0.5, 0.0, 0.0))
            targetLoc.add(-0.5, 0.0, 0.0);
        if (isWall(-1.0, 0.0, 0.0) || isWall(-0.5, 0.0, 0.0))
            targetLoc.add(0.5, 0.0, 0.0);

        // Y
        if (isWall(0.0, 1.0, 0.0) || isWall(0.0, 0.5, 0.0))
            targetLoc.add(0.0, -0.5, 0.0);
        if (isWall(0.0, -1.0, 0.0) || isWall(0.0, -0.5, 0.0))
            targetLoc.add(0.0, 0.5, 0.0);

        // Z
        if (isWall(0.0, 0.0, 1.0) || isWall(0.0, 0.0, 0.5))
            targetLoc.add(0.0, 0.0, -0.5);
        if (isWall(0.0, 0.0, -1.0) || isWall(0.0, 0.0, -0.5))
            targetLoc.add(0.0, 0.0, 0.5);

        if (this.locChanged) player.teleport(targetLoc);
    }

    private boolean isWall(double x, double y, double z) {
        if (this.sourceLoc.clone().add(x, y, z).getBlock().getType() == Material.AIR) return false;
        this.locChanged = true;
        return true;
    }

    @EventHandler
    private void on(PlayerQuitEvent event) {
        this.pearlsLaunchPositions.remove(event.getPlayer());
    }
}
