package ru.flexmine.flexsurvival.utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

public class ItemUtils {
    public static ItemStack createItem(Material material, String name, String... lore) {
        ItemStack item = new ItemStack(material);
        return createItem(item, name, Arrays.asList(lore));
    }

    public static ItemStack createItem(Material material, int subId, String name, String... lore) {
        ItemStack item = new ItemStack(material);
        item.setDurability((short) subId);
        return createItem(item, name, Arrays.asList(lore));
    }

    public static ItemStack createItem(Material material, String name, List<String> lore) {
        ItemStack item = new ItemStack(material);
        return createItem(item, name, lore);
    }

    public static ItemStack createItem(Material material, int subId, String name, List<String> lore) {
        ItemStack item = new ItemStack(material, subId);
        item.setDurability((short) subId);
        return createItem(item, name, lore);
    }

    private static ItemStack createItem(ItemStack item, String name, List<String> lore) {
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        lore.forEach(a -> a = ChatColor.translateAlternateColorCodes('&', a));
        itemMeta.setLore(lore);
        item.setItemMeta(itemMeta);
        return item;
    }
}
